<div class="sect1">
<h2 id="_insecure_ssltls_configuration">Insecure SSL/TLS configuration</h2>
<div class="sectionbody">
<div class="sect2">
<h3 id="_description">Description</h3>
<div class="paragraph">
<p>We noticed the following weaknesses in the SSL/TLS configuration that may allow
an attacker to intercept sensitive data or perform unauthorized actions:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>TLS 1.0 (published in 1999) is deprecated, there are various security issues
with TLS 1.0 (including the BEAST and POODLE attack).
TLS 1.0 does not support modern, strong cipher suites.
Furthermore, TLS 1.0 is no longer acceptable for PCI Compliance.
These vulnerabilities can enable Man-in-the-Middle attacks against TLS.
Multiple systems support TLS 1.0:</p>
<div class="ulist">
<ul>
<li>
<p>dh480.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>dh-small-subgroup.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>rc4-md5.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>self-signed.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>untrusted-root.badssl.com/104.154.89.105 on TCP port 443.</p>
</li>
</ul>
</div>
</li>
<li>
<p>Modern browsers have support for so-called Forward Secrecy ciphers using the
DHE/ECDHE algorithm.
These ciphers limit the impact of a compromised private key by still
maintaining the confidentiality of data exchanged in prior sessions.
We noticed a lack of support of Forward Secrecy ciphers:</p>
<div class="ulist">
<ul>
<li>
<p>rc4-md5.badssl.com/104.154.89.105 on TCP port 443.</p>
</li>
</ul>
</div>
</li>
<li>
<p>We noticed the presence of RC4, which is vulnerable to known security
weaknesses (such as Bar mitzvah).
When RC4 encryption is used, it is possible to recover a limited amount of
plaintext data. (Refer to
<a href="https://community.qualys.com/blogs/securitylabs/2013/03/19/rc4-in-tls-is-broken-now-what" class="bare">https://community.qualys.com/blogs/securitylabs/2013/03/19/rc4-in-tls-is-broken-now-what</a>
for more details):</p>
<div class="ulist">
<ul>
<li>
<p>rc4-md5.badssl.com/104.154.89.105 on TCP port 443.</p>
</li>
</ul>
</div>
</li>
<li>
<p>We noticed the presence of a cryptographic vulnerability, called the BREACH
attack (refer to
<a href="https://blog.qualys.com/ssllabs/2013/08/07/defending-against-the-breach-attack" class="bare">https://blog.qualys.com/ssllabs/2013/08/07/defending-against-the-breach-attack</a>
for more details):</p>
<div class="ulist">
<ul>
<li>
<p>dh480.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>dh-small-subgroup.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>rc4-md5.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>self-signed.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>untrusted-root.badssl.com/104.154.89.105 on TCP port 443.</p>
</li>
</ul>
</div>
</li>
<li>
<p>We noticed the presence of block ciphers with 64-bit blocks
in one or more cipher suites.
Systems using these ciphers are affected by a vulnerability known as SWEET32
due to the use of weak 64-bit block ciphers.
A Man-in-the-Middle attacker who has sufficient resources can exploit this
vulnerability, via a 'birthday' attack.
The attacker detects a collision that leaks the XOR between the fixed secret
and a known plain-text.
This allows for the disclosure of the secret text, such as secure HTTPS
cookies, and possibly resulting in the hijacking of an authenticated session:</p>
<div class="ulist">
<ul>
<li>
<p>self-signed.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>untrusted-root.badssl.com/104.154.89.105 on TCP port 443.</p>
</li>
</ul>
</div>
</li>
</ul>
</div>
</div>
<div class="sect2">
<h3 id="_likelihood">Likelihood</h3>
<div class="paragraph">
<p>The attacker would need to be on designated networks (i.e. on the path between
the user and the server) to abuse these issues.
Advanced technical skills are required to intercept, manipulate and decrypt the
encrypted traffic.</p>
</div>
<div class="paragraph">
<p>Furthermore, the user&#8217;s client software has to be outdated                                     or misconfigured in order to be vulnerable to the BEAST attack.</p>
</div>
</div>
<div class="sect2">
<h3 id="_impact">Impact</h3>
<div class="paragraph">
<p>Successful exploitation could allow an attacker to intercept a user&#8217;s password.
This allows the attacker to impersonate the user, and perform any action on
behalf of that user.
However, the vulnerability only affects users for which the attacker can
intercept SSL/TLS traffic.</p>
</div>
</div>
<div class="sect2">
<h3 id="_recommendation">Recommendation</h3>
<div class="paragraph">
<p>We recommend improving the SSL/TLS configuration of the affected
systems.
The most secure configuration at the moment is only supporting TLS
1.3 or TLS 1.2 with secure ciphers.
For example, ciphers that use ECDHE / DHE for key agreement
(Elliptic curve Diffie-Hellman).
The ciphers should also strictly use authenticated encryption (AES
in GCM mode or the stream cipher ChaCha20 with Poly1305).
And finally, the configuration should not allow older protocols
such as SSL version 2, SSL version 3, TLS 1.0 and TLS 1.1.</p>
</div>
<div class="paragraph">
<p>The PCI DSS standard requires (since June 2018) that all sites
which accept credit card payments to disable support for TLS 1.0
or lower.
Furthermore, Microsoft, Google, and Apple announced that TLS 1.0
and 1.1 will be disabled in their browsers at the beginning of
2020.
However, some older browsers do not support new versions such as
TLS 1.2 or TLS 1.3.
Refer to <a href="https://www.ssllabs.com/ssltest/clients.html" class="bare">https://www.ssllabs.com/ssltest/clients.html</a> for a list
of supported browsers.
This means that users with older browsers cannot connect to the
application.
Therefore, we recommend researching whether users are still using
older clients before disabling TLS 1.0 and TLS 1.1.</p>
</div>
<div class="paragraph">
<p>Refer to
<a href="https://www.ssllabs.com/projects/best-practices/index.html" class="bare">https://www.ssllabs.com/projects/best-practices/index.html</a> for a
detailed guide on how to configure SSL in a secure manner.</p>
</div>
<div class="paragraph">
<p>Furthermore, we recommend to:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>If possible, disable support for the TLS 1.0 protocol because it exposes ;
users to the BEAST attack;</p>
</li>
<li>
<p>Enable Forward Secrecy ciphers using the DHE/ECDHE algorithm to support ;
Forward Secrecy in modern browsers;</p>
</li>
<li>
<p>To prevent RC4 vulnerabilities (such as Bar mitzvah attack) enable support ;
for TLS 1.2 and GCM;</p>
</li>
<li>
<p>Disable gzip HTTP compression to prevent the BREACH attack;</p>
</li>
<li>
<p>Disable support for legacy 64-bit block ciphers to prevent the SWEET32 ;
attack.</p>
</li>
</ul>
</div>
<div style="page-break-after: always;"></div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_weaknesses_in_x_509_certificates">Weaknesses in X.509 certificates</h2>
<div class="sectionbody">
<div class="sect2">
<h3 id="_description_2">Description</h3>
<div class="paragraph">
<p>X.509 is a standard that defines the format of public key certificates. X.509
certificates are used in many Internet protocols, including TLS/SSL, which are
protocols used to transmit data over the network through an encrypted channel.
Before data is exchanged the server authenticates itself to the client based on
a certificate. This certificate ensures the authenticity of the server.</p>
</div>
<div class="paragraph">
<p>We noticed the following weaknesses in the X.509 certificates in use:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>The following systems use an X.509 certificate which is not issued by a
trusted third party:</p>
<div class="ulist">
<ul>
<li>
<p>self-signed.badssl.com/104.154.89.105 on TCP port 443.</p>
</li>
</ul>
</div>
</li>
</ul>
</div>
</div>
<div class="sect2">
<h3 id="_likelihood_2">Likelihood</h3>
<div class="paragraph">
<p>Because of this warning the user will grow accustomed to accepting security
warnings.
This increases the likelihood that an attacker can perform a successful
man-in-the-middle attack.
The attacker would need to be on designated networks (i.e. on the path between
the user and the server) to abuse these issues.
Advanced technical skills are required to perform a man-in-the-middle attack.</p>
</div>
</div>
<div class="sect2">
<h3 id="_impact_2">Impact</h3>
<div class="paragraph">
<p>Successful exploitation could allow an attacker to intercept a user"s password.
This allows the attacker to impersonate the user, and perform any action on
behalf of that user.
However, the vulnerability only affects users for which the attacker can
intercept SSL/TLS traffic.</p>
</div>
</div>
<div class="sect2">
<h3 id="_recommendation_2">Recommendation</h3>
<div class="paragraph">
<p>We recommend obtaining a new X.509 certificate, which addresses the issues
identified:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>The certificate should be obtained from a trusted party that is trusted by ;
all major browsers.;
If the system is only used within the internal network, an internal ;
certificate server should be used to securely distribute trusted ;
certificates;</p>
</li>
<li>
<p>Make sure that certificates in chain use RSA keys of at least 2048 bits, ;
and reissue any certificates signed by the old certificate.</p>
</li>
</ul>
</div>
<div style="page-break-after: always;"></div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_usage_of_wildcard_x_509_certificates">Usage of wildcard X.509 certificates</h2>
<div class="sectionbody">
<div class="sect2">
<h3 id="_description_3">Description</h3>
<div class="paragraph">
<p>X.509 is a standard that defines the format of public key certificates.
X.509 certificates are used in many Internet protocols, including TLS/SSL, which
are protocols used to transmit data over the network through an encrypted
channel.
Before data is exchanged the server authenticates itself to the client based on
a certificate. This certificate ensures the authenticity of the server.</p>
</div>
<div class="paragraph">
<p>We noticed X.509 certificates where the name contains a wildcard.
The use of wildcard certificates poses an additional security risk.
In the case the private key associated with the certificate is compromised, an
attacker can impersonate any system within the wildcard’s scope.</p>
</div>
<div class="paragraph">
<p>The following systems using wildcard X.509 certificates were identified:</p>
</div>
<div class="ulist">
<ul>
<li>
<p>dh480.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>dh-small-subgroup.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>rc4-md5.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>self-signed.badssl.com/104.154.89.105 on TCP port 443;</p>
</li>
<li>
<p>untrusted-root.badssl.com/104.154.89.105 on TCP port 443.</p>
</li>
</ul>
</div>
</div>
<div class="sect2">
<h3 id="_likelihood_3">Likelihood</h3>
<div class="paragraph">
<p>In order to abuse the wildcard certificates, an attacker would have to
compromise the system where the private key associated with the certificate is
stored.
This would require the attacker to identify and exploit a vulnerability in this
system.
Advanced technical skills are required to perform such an attack.</p>
</div>
</div>
<div class="sect2">
<h3 id="_impact_3">Impact</h3>
<div class="paragraph">
<p>Usage of wildcard X.509 certificates increases the impact of a successful attack
on the certificate.
In case it is compromised, the attacker can use it to perform a
man-in-the-middle attack on any system within the wildcard’s scope, instead of
on a single system.
This offers a platform for further attacks.</p>
</div>
</div>
<div class="sect2">
<h3 id="_recommendation_3">Recommendation</h3>
<div class="paragraph">
<p>We recommend using a unique X.509 certificate for each system, which requires
encrypted communication.
The usage of a unique certificate per system limits the impact of a compromise.
Also, refer to <a href="https://www.ssllabs.com/projects/best-practices/index.html" class="bare">https://www.ssllabs.com/projects/best-practices/index.html</a> for a
detailed guide on how to configure TLS in a secure manner.</p>
</div>
<div style="page-break-after: always;"></div>
</div>
</div>
</div>