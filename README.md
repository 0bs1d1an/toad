# Testssl.sh Observations in AsciiDoc (toad)

The toad accepts testssl.sh JSON output, and converts this to report observations in AsciiDoc. It then converts the AsciiDoc to either "flat" HTML (without fancy formatting), or "rich" HTML (with fancy formatting).

## Requirements

1. python (dev-lang/python);
2. testssl (net-analyzer/testssl);
3. asciidoctor (dev-ruby/asciidoctor).

## Usage

```
$ toad.py -h
usage: toad.py [-h] [-a] [-f] [-i INPUT [INPUT ...]] [-o OUTPUT]

A tool to convert testssl.sh JSON files to observations in AsciiDoc.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT [INPUT ...], --input INPUT [INPUT ...]
                        Specify the input testssl.sh JSON file(s).
  -o OUTPUT, --output OUTPUT
                        Specify the output AsciiDoc file.
```

## Example

Scan a target system with testssl.sh, and save the result to a JSON file (and optionally HTML for later human inspection):

```
$ testssl.sh --jsonfile rc4-md5.badssl.com.json --htmlfile rc4-md5.badssl.com.html rc4-md5.badssl.com
```

Then, let's assume you scanned multiple targets and now have multiple JSON files in a directory called "example/input".
You wish to parse all these JSON files and save the AsciiDoc output to "example/output/output.adoc".
You may also wish to automatically render the AsciiDoc result to HTML.
You can achieve this by running the following command:

```
$ toad.py -a -i example/input/*.json -o example/output/output.adoc
```

Have a look in the example folder of this repository to inspect the result.
