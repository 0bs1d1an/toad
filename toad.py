#!/usr/bin/env python
import argparse
import json
import os
from subprocess import call

TOTAL_AMOUNT_OF_OBSERVATIONS = 0

def parser():
    modes = ['o', 'input']
    parser = argparse.ArgumentParser(description='A tool to convert testssl.sh JSON files to \
        observations in AsciiDoc.')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'), nargs='+', help='Specify the \
        input testssl.sh JSON files (e.g. *.json).')
    parser.add_argument('-o', '--output', help='Specify the output AsciiDoc file (e.g. output.adoc\
        ).')
    args = parser.parse_args()

    # Iterate over all input files, and appending each as an element to this array
    results_array = []

    for file in args.input:
        with open(file.name, 'r') as input_file:
            results_array.append(json.load(input_file))

    output_file_name = args.output

    def insecure_tls_ssl_configuration():
        global TOTAL_AMOUNT_OF_OBSERVATIONS
        TOTAL_AMOUNT_OF_OBSERVATIONS = 0
        with open(output_file_name, "w") as output_file:
            def finding_desc(id_name, dict_key, dict_value, desc_text):
                global TOTAL_AMOUNT_OF_OBSERVATIONS
                amount = 0
                for result in results_array:
                    for dict in result:
                        if dict['id'] == id_name and dict[dict_key] != dict_value:
                            amount =+ 1
                            TOTAL_AMOUNT_OF_OBSERVATIONS += 1
                            if TOTAL_AMOUNT_OF_OBSERVATIONS == 1:
                                for line in obs_header:
                                    print(line, file=output_file)

                if amount > 0:
                    for line in desc_text:
                        print(line, file=output_file)

                hosts_array = []
                for result in results_array:
                    for dict in result:
                        if dict['id'] == id_name and dict[dict_key] != dict_value:
                            hosts_array.append('** ' + dict['ip'] + ' on TCP port ' + dict['port'])

                if hosts_array:
                    print(*hosts_array, sep=";\n", end="", file=output_file)
                    print(".", file=output_file)

            sslv2_desc_text = [
                "* We noticed the presence of SSL version 2, which contains known cryptographic ",
                "  weaknesses including the DROWN attack:"]
            sslv3_desc_text = [
                "* We noticed the presence of SSL version 3, which contains known cryptographic ",
                "  weaknesses including the POODLE attack (refer to ",
                "  https://www.openssl.org/~bodo/ssl-poodle.pdf for more details):"]
            tls1_desc_text = [
                "* TLS 1.0 (published in 1999) is deprecated, there are various security issues ",
                "  with TLS 1.0 (including the BEAST and POODLE attack).",
                "  TLS 1.0 does not support modern, strong cipher suites.",
                "  Furthermore, TLS 1.0 is no longer acceptable for PCI Compliance.",
                "  These vulnerabilities can enable Man-in-the-Middle attacks against TLS.",
                "  Multiple systems support TLS 1.0:"]
            pfs_desc_text = [
                "* Modern browsers have support for so-called Forward Secrecy ciphers using the ",
                "  DHE/ECDHE algorithm.",
                "  These ciphers limit the impact of a compromised private key by still ",
                "  maintaining the confidentiality of data exchanged in prior sessions.",
                "  We noticed a lack of support of Forward Secrecy ciphers:"]
            anull_desc_text = [
                "* We noticed the presence of ciphers which are based on the Anonymous ",
                "  Diffie-Hellman algorithm.",
                "  This algorithm provides confidentiality but does not authenticate either of ",
                "  the communicating parties.",
                "  This allows an attacker to impersonate the user or server:"]
            crime_desc_text = [
                "* We noticed the presence of a cryptographic vulnerability, called the CRIME ",
                "  attack (refer to ",
                "  https://blog.qualys.com/ssllabs/2012/09/14/crime-information-leakage-attack-against-ssltls",
                "  for more details):"]
            secure_renego_desc_text = [
                "* We noticed the presence of a cryptographic vulnerability, called the insecure ",
                "server renegotiation weakness:"]
            secure_client_renego_desc_text = [
                "* We noticed the presence of a Man-in-the-Middle vulnerability known as legacy ",
                "  client-initiated SSL renegotiations (refer to ",
                "  https://www.g-sec.lu/practicaltls.pdf for more details):"]
            rc4_desc_text = [
                "* We noticed the presence of RC4, which is vulnerable to known security ",
                "  weaknesses (such as Bar mitzvah).",
                "  When RC4 encryption is used, it is possible to recover a limited amount of ",
                "  plaintext data. (Refer to ",
                "  https://community.qualys.com/blogs/securitylabs/2013/03/19/rc4-in-tls-is-broken-now-what",
                "  for more details):"]
            breach_desc_text = [
                "* We noticed the presence of a cryptographic vulnerability, called the BREACH ",
                "  attack (refer to",
                "  https://blog.qualys.com/ssllabs/2013/08/07/defending-against-the-breach-attack",
                "  for more details):"]
            robot_desc_text = [
                "* We noticed the presence of a cryptographic vulnerability, called the ROBOT ",
                "  attack. ROBOT allows an attacker to obtain the RSA key necessary to decrypt ",
                "  TLS traffic under certain conditions:"]
            freak_desc_text = [
                "* We noticed the presence of a cryptographic vulnerability, called the FREAK ",
                "  attack. Refer to ",
                "  https://geekflare.com/test-freak-attack-cve-2015-0204-and-fix/ for more ",
                "  details:"]
            sweet32_desc_text = [
                "* We noticed the presence of block ciphers with 64-bit blocks ",
                "  in one or more cipher suites.",
                "  Systems using these ciphers are affected by a vulnerability known as SWEET32 ",
                "  due to the use of weak 64-bit block ciphers.",
                "  A Man-in-the-Middle attacker who has sufficient resources can exploit this ",
                "  vulnerability, via a \'birthday\' attack.",
                "  The attacker detects a collision that leaks the XOR between the fixed secret ",
                "  and a known plain-text.",
                "  This allows for the disclosure of the secret text, such as secure HTTPS ",
                "  cookies, and possibly resulting in the hijacking of an authenticated session:"]

            obs_header = [
                '== Insecure SSL/TLS configuration',
                "",
                "=== Description",
                "",
                "We noticed the following weaknesses in the SSL/TLS configuration that may allow ",
                "an attacker to intercept sensitive data or perform unauthorized actions:",
                ""]

            finding_desc("SSLv2", "severity", "OK", sslv2_desc_text)
            finding_desc("SSLv3", "severity", "OK", sslv3_desc_text)
            finding_desc("TLS1", "finding", "not offered", tls1_desc_text)
            finding_desc("PFS", "severity", "OK", pfs_desc_text)
            finding_desc("cipherlist_aNULL", "severity", "OK", anull_desc_text)
            finding_desc("CRIME_TLS", "severity", "OK", crime_desc_text)
            finding_desc("secure_renego", "severity", "OK", secure_renego_desc_text)
            finding_desc("secure_client_renego", "severity", "OK", secure_client_renego_desc_text)
            finding_desc("RC4", "severity", "OK", rc4_desc_text)
            finding_desc("BREACH", "severity", "OK", breach_desc_text)
            finding_desc("ROBOT", "severity", "OK", robot_desc_text)
            finding_desc("FREAK", "severity", "OK", freak_desc_text)
            finding_desc("SWEET32", "severity", "OK", sweet32_desc_text)

            likelihood = [
                "",
                "=== Likelihood",
                "",
                "The attacker would need to be on designated networks (i.e. on the path between ",
                "the user and the server) to abuse these issues.",
                "Advanced technical skills are required to intercept, manipulate and decrypt the ",
                "encrypted traffic.",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in likelihood:
                    print(line, file=output_file)

                # if BEAST on any host; then print specific BEAST likelihood
                def beast():
                    for result in results_array:
                        for dict in result:
                            if dict['id'] == 'TLS1' and dict['severity'] != 'OK':
                                print("Furthermore, the user\'s client software has to be outdated \
                                    or misconfigured in order to be vulnerable to the BEAST \
                                    attack.",
                                      file=output_file)
                                return

                beast()

            impact = [
                "",
                "=== Impact",
                "",
                "Successful exploitation could allow an attacker to intercept a user\'s password.",
                "This allows the attacker to impersonate the user, and perform any action on ",
                "behalf of that user.",
                "However, the vulnerability only affects users for which the attacker can ",
                "intercept SSL/TLS traffic.",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in impact:
                    print(line, file=output_file)

            recommendation = [
                "=== Recommendation",
                "",
                "We recommend improving the SSL/TLS configuration of the affected systems.",
                "The most secure configuration at the moment is only supporting TLS 1.3 or TLS ",
                "1.2 with secure ciphers.",
                "For example, ciphers that use ECDHE / DHE for key agreement (Elliptic curve ",
                "Diffie-Hellman).",
                "The ciphers should also strictly use authenticated encryption (AES in GCM mode ",
                "or the stream cipher ChaCha20 with Poly1305).",
                "And finally, the configuration should not allow older protocols such as SSL ",
                "version 2, SSL version 3, TLS 1.0 and TLS 1.1.",
                "",
                "The PCI DSS standard requires (since June 2018) that all sites which accept ",
                "credit card payments to disable support for TLS 1.0 or lower.",
                "Furthermore, Microsoft, Google, and Apple announced that TLS 1.0 and 1.1 will be ",
                "disabled in their browsers at the beginning of 2020.",
                "However, some older browsers do not support new versions such as TLS 1.2 or TLS ",
                "1.3.",
                "Refer to https://www.ssllabs.com/ssltest/clients.html for a list of supported ",
                "browsers.",
                "This means that users with older browsers cannot connect to the application.",
                "Therefore, we recommend researching whether users are still using older clients ",
                "before disabling TLS 1.0 and TLS 1.1.",
                "",
                "Refer to https://www.ssllabs.com/projects/best-practices/index.html for a ",
                "detailed guide on how to configure SSL in a secure manner.",
                "",
                "Furthermore, we recommend to:",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in recommendation:
                    print(line, file=output_file)

                def finding_rec(id_name, dict_key, dict_value, rec_text):
                    for result in results_array:
                        for dict in result:
                            if dict['id'] == id_name and dict[dict_key] != dict_value:
                                for line in rec_text:
                                    rec_array.append(line)
                                return

                sslv2_rec_text = [
                    "* Disable support for the SSL version 2 protocol"]
                sslv3_rec_text = [
                    "* Disable support for the SSL version 3 protocol"]
                tls1_rec_text = [
                    "* If possible, disable support for the TLS 1.0 protocol because it exposes ",
                    "  users to the BEAST attack"]
                pfs_rec_text = [
                    "* Enable Forward Secrecy ciphers using the DHE/ECDHE algorithm to support ",
                    "  Forward Secrecy in modern browsers"]
                anull_rec_text = [
                    "* Avoid the usage of ciphers which are based on the Anonymous ",
                    "  Diffie-Hellman algorithm (ADH)"]
                crime_rec_text = ["* Disable TLS compression to prevent the CRIME attack"]
                secure_reneg_rec_text = [
                    "* Update the SSL libraries used to the latest version to prevent the ",
                    "  insecure renegotiation attack.",
                    "  As a workaround renegotiation can be disabled completely in case it is not ",
                    "  in use"]
                secure_client_reneg_rec_text = [
                    "* Disable client initiated SSL renegotiation to prevent man-in-the-middle ",
                    "  attacks"]
                rc4_rec_text = [
                    "* To prevent RC4 vulnerabilities (such as Bar mitzvah attack) enable support ",
                    "  for TLS 1.2 and GCM"]
                breach_rec_text = [
                    "* Disable gzip HTTP compression to prevent the BREACH attack"]
                robot_rec_text = [
                    "* Disable all ciphers that start with TLS_RSA. ROBOT only affects TLS cipher ",
                    "  modes that use RSA encryption"]
                freak_rec_text = [
                    "* Disable support for export cipher suites to prevent the FREAK attack"]
                sweet32_rec_text = [
                    "* Disable support for legacy 64-bit block ciphers to prevent the SWEET32 ",
                    "  attack"]

                rec_array = []

                finding_rec("SSLv2", "severity", "OK", sslv2_rec_text)
                finding_rec("SSLv3", "severity", "OK", sslv3_rec_text)
                finding_rec("TLS1", "finding", "not offered", tls1_rec_text)
                finding_rec("PFS", "severity", "OK", pfs_rec_text)
                finding_rec("cipherlist_aNULL", "severity", "OK", anull_rec_text)
                finding_rec("CRIME_TLS", "severity", "OK", crime_rec_text)
                finding_rec("secure_renego", "severity", "OK", secure_reneg_rec_text)
                finding_rec("secure_client_renego", "severity", "OK", \
                    secure_client_reneg_rec_text)
                finding_rec("RC4", "severity", "OK", rc4_rec_text)
                finding_rec("BREACH", "severity", "OK", breach_rec_text)
                finding_rec("ROBOT", "severity", "OK", robot_rec_text)
                finding_rec("FREAK", "severity", "OK", freak_rec_text)
                finding_rec("SWEET32", "severity", "OK", sweet32_rec_text)

                if rec_array:
                    print(*rec_array, sep=";\n", end="", file=output_file)
                    print(".", file=output_file)

                print("\n<<<\n", file=output_file)

    def weaknesses_in_x509_certificates():
        global TOTAL_AMOUNT_OF_OBSERVATIONS
        TOTAL_AMOUNT_OF_OBSERVATIONS = 0
        with open(output_file_name, "a") as output_file:
            def finding_desc(id_name, dict_key, dict_value, desc_text):
                global TOTAL_AMOUNT_OF_OBSERVATIONS
                amount = 0
                for result in results_array:
                    for dict in result:
                        if dict['id'] == id_name:
                            if dict[dict_key] != dict_value:
                                if dict[dict_key] != "INFO":
                                    amount =+ 1
                                    TOTAL_AMOUNT_OF_OBSERVATIONS += 1
                                    if TOTAL_AMOUNT_OF_OBSERVATIONS == 1:
                                        for line in obs_header:
                                            print(line, file=output_file)

                if amount > 0:
                    for line in desc_text:
                        print(line, file=output_file)

                hosts_array = []
                for result in results_array:
                    for dict in result:
                        if dict['id'] == id_name:
                            if dict[dict_key] != dict_value:
                                if dict[dict_key] != "INFO":
                                    hosts_array.append('** ' + dict['ip'] + ' on TCP port ' +  \
                                        dict['port'])
                if hosts_array:
                    print(*hosts_array, sep=";\n", end="", file=output_file)
                    print(".", file=output_file)

            cn_desc_text = [
                "* The following systems use an X.509 certificate where the certificate name does ",
                "  not match the name of the server:"]
            ca_desc_text = [
                "* The following systems use an X.509 certificate which is not issued by a ",
                "  trusted third party:"]
            expire_desc_text = [
                "* The following systems use an X.509 certificate which has expired:"]
            signature_algo_desc_text = [
                "* The following systems use an X.509 certificate that has been signed using a ",
                "  cryptographically weak hashing algorithm. These algorithms are known to be ",
                "  vulnerable to collision attacks:"]
            key_size_desc_text = [
                "* The following systems use RSA keys of less than 2048 bits in the X.509 ",
                "  certificate chain"]
            san_desc_text = [
                "* The following systems use an X.509 certificate without specifying the “Subject ",
                "  Alternative Names” (SAN) parameter. Google Chrome provides a security warning ",
                "  for these certificates:"]

            obs_header = [
                "== Weaknesses in X.509 certificates",
                "",
                "=== Description",
                "",
                "X.509 is a standard that defines the format of public key certificates. X.509 ",
                "certificates are used in many Internet protocols, including TLS/SSL, which are ",
                "protocols used to transmit data over the network through an encrypted channel.",
                "Before data is exchanged the server authenticates itself to the client based on ",
                "a certificate. This certificate ensures the authenticity of the server.",
                "",
                "We noticed the following weaknesses in the X.509 certificates in use:",
                ""]

            finding_desc("cert_commonName", "severity", "OK", cn_desc_text)
            finding_desc("cert_caIssuers", "severity", "INFO", ca_desc_text)
            finding_desc("cert_expirationStatus", "severity", "OK", expire_desc_text)
            finding_desc("cert_signatureAlgorithm", "severity", "OK", signature_algo_desc_text)
            finding_desc("cert_keySize", "severity", "OK", key_size_desc_text)
            finding_desc("cert_subjectAltName", "severity", "INFO", san_desc_text)

            likelihood = [
                "",
                "=== Likelihood",
                "",
                "Because of this warning the user will grow accustomed to accepting security ",
                "warnings.",
                "This increases the likelihood that an attacker can perform a successful ",
                "man-in-the-middle attack.",
                "The attacker would need to be on designated networks (i.e. on the path between ",
                "the user and the server) to abuse these issues.",
                "Advanced technical skills are required to perform a man-in-the-middle attack.",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in likelihood:
                    print(line, file=output_file)

                # if weak cert signature algo on any host; then print its specific likelihood
                def weak_sign_algo():
                    for result in results_array:
                        for dict in result:
                            if dict['id'] == 'cert_signatureAlgorithm' and dict['severity'] != 'OK':
                                print("For the weak hashing algorithms, an attacker would need to \
                                    generate another certificate with the same digital \
                                    signature.",
                                      file=output_file)
                                print("This requires advanced technical skills.", file=output_file)
                                return

                weak_sign_algo()

            impact = [
                "=== Impact",
                "",
                "Successful exploitation could allow an attacker to intercept a user\"s password.",
                "This allows the attacker to impersonate the user, and perform any action on ",
                "behalf of that user.",
                "However, the vulnerability only affects users for which the attacker can ",
                "intercept SSL/TLS traffic.",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in impact:
                    print(line, file=output_file)

            recommendation = [
                "=== Recommendation",
                "",
                "We recommend obtaining a new X.509 certificate, which addresses the issues ",
                "identified:",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in recommendation:
                    print(line, file=output_file)

                def finding_rec(id_name, dict_key, dict_value, rec_text):
                    for result in results_array:
                        for dict in result:
                            if dict['id'] == id_name:
                                if dict[dict_key] != dict_value or not "INFO":
                                    for line in rec_text:
                                        rec_array.append(line)
                                    return

                cn_rec_text = [
                    "* The certificate name should match the fully qualified domain name of the ",
                    "  server it is used for"]
                ca_rec_text = [
                    "* The certificate should be obtained from a trusted party that is trusted by ",
                    "  all major browsers.",
                    "  If the system is only used within the internal network, an internal ",
                    "  certificate server should be used to securely distribute trusted ",
                    "  certificates"]
                expire_rec_text = [
                    "* The certificate should be renewed in a timely manner to avoid certificate ",
                    "  warnings due to certificate expiry.",
                    "  We recommend to automate your certificate life-cycle to pro-actively renew ",
                    "  expiring certificates "]
                signature_algo_rec_text = [
                    "* Avoid signing certificates with a weak hashing algorithm, such as MD2, ",
                    "  MD4, MD5, and SHA-1.",
                    "  Instead, use stronger hashing algorithms, such as SHA-256"]
                key_size_rec_text = [
                    "* Make sure that certificates in chain use RSA keys of at least 2048 bits, ",
                    "  and reissue any certificates signed by the old certificate"]
                san_rec_text = [
                    "* The “Subject Alternative Names” parameter should be specified in the X.509 ",
                    "  certificate"]

                rec_array = []

                finding_rec("cert_commonName", "severity", "OK", cn_rec_text)
                finding_rec("cert_caIssuers", "severity", "INFO", ca_rec_text)
                finding_rec("cert_expirationStatus", "severity", "OK", expire_rec_text)
                finding_rec("cert_signatureAlgorithm", "severity", "OK", signature_algo_rec_text)
                finding_rec("cert_keySize", "severity", "OK", key_size_rec_text)
                finding_rec("cert_subjectAltName", "severity", "INFO", san_rec_text)

                if rec_array:
                    print(*rec_array, sep=";\n", end="", file=output_file)
                    print(".", file=output_file)

                print("\n<<<\n", file=output_file)

    def wildcard_x509_certs():
        global TOTAL_AMOUNT_OF_OBSERVATIONS
        TOTAL_AMOUNT_OF_OBSERVATIONS = 0
        with open(output_file_name, "a") as output_file:
            def list_hosts():
                global TOTAL_AMOUNT_OF_OBSERVATIONS
                amount = 0
                hosts_array = []
                for result in results_array:
                    for dict in result:
                        if dict['id'] == "cert_commonName" and "*" in dict['finding']:
                            hosts_array.append('* ' + dict['ip'] + ' on TCP port ' + dict['port'])
                            TOTAL_AMOUNT_OF_OBSERVATIONS += 1
                            if TOTAL_AMOUNT_OF_OBSERVATIONS == 1:
                                for line in obs_header:
                                    print(line, file=output_file)

                if hosts_array:
                    print(*hosts_array, sep=";\n", end="", file=output_file)
                    print(".", file=output_file)

            obs_header = [
                "== Usage of wildcard X.509 certificates",
                "",
                "=== Description",
                "",
                "X.509 is a standard that defines the format of public key certificates.",
                "X.509 certificates are used in many Internet protocols, including TLS/SSL, which ",
                "are protocols used to transmit data over the network through an encrypted ",
                "channel.",
                "Before data is exchanged the server authenticates itself to the client based on ",
                "a certificate. This certificate ensures the authenticity of the server.",
                "",
                "We noticed X.509 certificates where the name contains a wildcard.",
                "The use of wildcard certificates poses an additional security risk.",
                "In the case the private key associated with the certificate is compromised, an ",
                "attacker can impersonate any system within the wildcard’s scope.",
                "",
                "The following systems using wildcard X.509 certificates were identified:",
                ""]

            list_hosts()

            likelihood = [
                "",
                "=== Likelihood",
                "",
                "In order to abuse the wildcard certificates, an attacker would have to ",
                "compromise the system where the private key associated with the certificate is ",
                "stored.",
                "This would require the attacker to identify and exploit a vulnerability in this ",
                "system.",
                "Advanced technical skills are required to perform such an attack.",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in likelihood:
                    print(line, file=output_file)

            impact = [
                "=== Impact",
                "",
                "Usage of wildcard X.509 certificates increases the impact of a successful attack ",
                "on the certificate.",
                "In case it is compromised, the attacker can use it to perform a ",
                "man-in-the-middle attack on any system within the wildcard’s scope, instead of ",
                "on a single system.",
                "This offers a platform for further attacks.",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in impact:
                    print(line, file=output_file)

            recommendation = [
                "=== Recommendation",
                "",
                "We recommend using a unique X.509 certificate for each system, which requires ",
                "encrypted communication.",
                "The usage of a unique certificate per system limits the impact of a compromise.",
                "Also, refer to https://www.ssllabs.com/projects/best-practices/index.html for a ",
                "detailed guide on how to configure TLS in a secure manner.",
                ""]

            if TOTAL_AMOUNT_OF_OBSERVATIONS > 0:
                for line in recommendation:
                    print(line, file=output_file)

                print("\n<<<\n", file=output_file)

    insecure_tls_ssl_configuration()
    weaknesses_in_x509_certificates()
    wildcard_x509_certs()

    call(["asciidoctor", output_file_name, '-o', os.path.splitext(output_file_name)[0] + \
        '-rich.html'])
    call(["asciidoctor", '-s', output_file_name, '-o', os.path.splitext(output_file_name)[0] + \
        '-flat.html'])

parser()
